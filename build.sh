#!/bin/bash
LIBPULSE_FLAGS=$(pkg-config libpulse --cflags --libs)
if [[ $? != 0 ]]; then
  LIBPULSE_FLAGS="-D_REENTRANT -lpulse"
fi
clang -O2 -Wno-writable-strings $LIBPULSE_FLAGS -lX11 -lXrandr volume.c -o vol_dzen
