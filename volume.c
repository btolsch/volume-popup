#include <X11/extensions/Xrandr.h>
#include <errno.h>
#include <fcntl.h>
#include <pulse/pulseaudio.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#define global_variable static
#define internal static

typedef int8_t s8;
typedef int32_t s32;
typedef uint32_t u32;
typedef uint64_t u64;
typedef float f32;

#define ArrayCount(x) (sizeof(x) / sizeof(*(x)))

#define CHECK(x)        \
  {                     \
    if (!(x)) {         \
      __builtin_trap(); \
    }                   \
  }

#if 0
#define DLOG(...) printf(__VA_ARGS__);
#else
#define DLOG(...)
#endif

global_variable char* default_sink_name;
global_variable pa_cvolume volume;
global_variable int base_volume;

global_variable char kPipeFile[] = "/tmp/vol_dzen.pipe";
global_variable int kBarWidth = 200;
global_variable int kTimeoutSec = 1;
global_variable int kTimeoutUsec = 0;

internal void
context_notify_cb(pa_context* ctx, void* userdata) {
  int* connected = (int*)userdata;
  *connected = pa_context_get_state(ctx) == PA_CONTEXT_READY;
}

internal void
sink_info_cb(pa_context* ctx, const pa_sink_info* info, int eol, void* userdata) {
  if (info) {
    volume = info->volume;
    base_volume = info->base_volume;
  }
}

internal void
server_info_cb(pa_context* ctx, const pa_server_info* info, void* userdata) {
  default_sink_name = (char*)info->default_sink_name;
}

internal void
pulseaudio_wait_for_operation(pa_mainloop* mainloop, pa_operation* op) {
  while (1) {
    if (pa_operation_get_state(op) == PA_OPERATION_DONE) {
      break;
    }
    pa_mainloop_iterate(mainloop, 0, 0);
  }
}

internal void
pulseaudio_adjust_volume(pa_context* ctx, pa_mainloop* mainloop, s32 volume_adjust) {
  for (int i = 0; i < volume.channels; ++i) {
    volume.values[i] += base_volume * ((f32)volume_adjust / 100.0f);
  }
  pa_operation* vol_op = pa_context_set_sink_volume_by_name(ctx, default_sink_name, &volume, 0, 0);
  pulseaudio_wait_for_operation(mainloop, vol_op);
}

typedef struct {
  u32 width;
  u32 height;
  u32 x;
  u32 y;
} DisplayInfo;

internal u32
get_xrandr_display_info(DisplayInfo* infos, u32 info_size) {
  Display* display = XOpenDisplay(0);

  XRRScreenResources* screen_resources = XRRGetScreenResources(display, DefaultRootWindow(display));

  u32 display_count = 0;
  for (u32 i = 0; i < screen_resources->noutput && display_count < info_size; ++i) {
    XRROutputInfo* info = XRRGetOutputInfo(display, screen_resources, screen_resources->outputs[i]);
    if (info->mm_width > 0 && info->mm_height > 0) {
      XRRCrtcInfo* crtc = XRRGetCrtcInfo(display, screen_resources, info->crtc);
      DLOG("%.*s: %ux%u+%d+%d\n", info->nameLen, info->name, crtc->width, crtc->height, crtc->x,
           crtc->y);
      u32 index = display_count++;
      infos[index].width = crtc->width;
      infos[index].height = crtc->height;
      infos[index].x = (u32)crtc->x;
      infos[index].y = (u32)crtc->y;
      XRRFreeCrtcInfo(crtc);
    }
    XRRFreeOutputInfo(info);
  }
  XRRFreeScreenResources(screen_resources);

  return display_count;
}

internal void
update_dzen(int dzen_fd) {
  int vol = 100 * ((f32)pa_cvolume_avg(&volume) / (f32)base_volume) + 0.5f;
  int volume_width = vol * 2;
  dprintf(dzen_fd, "^fg(#1793d1)^r(%dx10)^fg(#000000)^r(%dx10)\n", volume_width,
          kBarWidth - volume_width);
}

internal void
accumulate_volume_adjust(int dzen_fd, pa_context* ctx, pa_mainloop* mainloop, s32 volume_adjust) {
  pulseaudio_adjust_volume(ctx, mainloop, volume_adjust);
  update_dzen(dzen_fd);
}

internal void
run_dzen(int fd, s8 volume_adjust) {
  pid_t child_pid;
  int pfds[2];
  pipe(pfds);
  if ((child_pid = fork())) {
    close(pfds[0]);

    pa_mainloop* mainloop = pa_mainloop_new();

    int connected = 0;
    pa_context* ctx = pa_context_new(pa_mainloop_get_api(mainloop), "vol_dzen");
    pa_context_set_state_callback(ctx, &context_notify_cb, &connected);
    int ret = pa_context_connect(ctx, 0, PA_CONTEXT_NOAUTOSPAWN, 0);
    if (ret >= 0) {
      while (!connected) {
        pa_mainloop_iterate(mainloop, 0, 0);
      }
      pa_operation* op = pa_context_get_server_info(ctx, &server_info_cb, 0);
      pulseaudio_wait_for_operation(mainloop, op);

      pa_operation* sink_info_op =
          pa_context_get_sink_info_by_name(ctx, default_sink_name, &sink_info_cb, 0);
      pulseaudio_wait_for_operation(mainloop, sink_info_op);

      pulseaudio_adjust_volume(ctx, mainloop, volume_adjust);

      struct timeval timeout;
      timeout.tv_sec = kTimeoutSec;
      timeout.tv_usec = kTimeoutUsec;
      fd_set fds;
      FD_ZERO(&fds);
      FD_SET(fd, &fds);
      update_dzen(pfds[1]);

      struct timespec start = {};
      struct timespec now = {};
      clock_gettime(CLOCK_MONOTONIC, &start);
      s32 net_adjust = 0;
      while ((ret = select(fd + 1, &fds, 0, 0, &timeout)) > 0) {
        DLOG("select() says there's a read\n");
        int byte_count = read(fd, &volume_adjust, 1);
        if (byte_count <= 0) {
          break;
        }
        net_adjust += volume_adjust;
        FD_CLR(fd, &fds);
        FD_SET(fd, &fds);
        timeout.tv_sec = kTimeoutSec;
        timeout.tv_usec = kTimeoutUsec;
        DLOG("read volume: %u\n", vol);

        clock_gettime(CLOCK_MONOTONIC, &now);
        u64 ms = ((now.tv_sec - start.tv_sec) * 1000000000ull + (now.tv_nsec - start.tv_nsec)) /
                 1000000ull;
        if (ms >= 33) {
          start = now;
          accumulate_volume_adjust(pfds[1], ctx, mainloop, net_adjust);
          net_adjust = 0;
        }
      }
      if (net_adjust) {
        accumulate_volume_adjust(pfds[1], ctx, mainloop, net_adjust);
      }
      pa_context_disconnect(ctx);
      flock(fd, LOCK_UN);
    }

    close(pfds[1]);
    int status;
    waitpid(child_pid, &status, 0);
  } else {
    close(pfds[1]);

    DisplayInfo infos[3] = {};
    int child_fds[3][2] = {};
    int child_pids[3] = {};
    u32 display_count = get_xrandr_display_info(infos, ArrayCount(infos));
    u32 i = 0;
    for (; i < display_count; ++i) {
      pipe(child_fds[i]);
      if ((child_pid = child_pids[i] = fork()) == 0) {
        break;
      }
    }
    if (child_pid == 0) {
      char x[10];
      char y[10];
      sprintf(x, "%d", infos[i].x + infos[i].width / 2 - 350 / 2);
      sprintf(y, "%d", infos[i].y + 50);
      char* args[] = {"dzen2", "-fg", "#888888", "-bg", "#222222", "-x", x,   "-y",
                      y,       "-w",  "250",     "-h",  "40",      "-p", "1", 0};
      close(pfds[0]);
      for (u32 j = 0; j < i; ++j) {
        close(child_fds[j][0]);
        close(child_fds[j][1]);
      }
      close(child_fds[i][1]);
      dup2(child_fds[i][0], STDIN_FILENO);

      execvp("dzen2", args);
    } else {
      for (u32 i = 0; i < display_count; ++i) {
        close(child_fds[i][0]);
      }
      char buf[100];
      while (1) {
        int ret = read(pfds[0], buf, sizeof(buf));
        if (ret > 0) {
          for (u32 i = 0; i < display_count; ++i) {
            write(child_fds[i][1], buf, ret);
          }
        } else if (ret < 0 && errno == EINTR) {
        } else {
          break;
        }
      }
      for (u32 i = 0; i < display_count; ++i) {
        close(child_fds[i][1]);
      }
      for (u32 i = 0; i < display_count; ++i) {
        int status;
        waitpid(child_pids[i], &status, 0);
      }
    }
  }
}

internal void
dzen_show_volume(int fd, s8 volume_adjust) {
  CHECK(fd != -1);
  int result = flock(fd, LOCK_EX | LOCK_NB);
  if (result == 0) {
    DLOG("got flock\n");
    run_dzen(fd, volume_adjust);
  } else if (result == -1 && errno == EWOULDBLOCK) {
    // TODO(btolsch): We write because we couldn't get the master read lock,
    // but we could still get SIGPIPE because master closes/crashes between
    // flock and the write.  So really we need a SIGPIPE handler that sends
    // us back through this logic, and maybe fails after N retries.
    DLOG("no flock, writing volume to fifo instead\n");
    write(fd, &volume_adjust, sizeof(volume));
  } else {
    DLOG("flock error\n");
    // TODO(btolsch): real error
  }
}

int
main(int argc, char** argv) {
  if (argc == 2) {
    s8 volume_adjust = 0;
    s8 i = 0;
    s8 sign = 1;
    if (argv[1][0] == '-') {
      sign = -1;
      ++i;
    }
    for (; argv[1][i] && i < 3; ++i) {
      volume_adjust = (argv[1][i] - '0') + volume_adjust * 10;
    }
    volume_adjust *= sign;
    int fd = open(kPipeFile, O_RDWR);
    if (fd == -1) {
      DLOG("making fifo\n");
      int fd = mkfifo(kPipeFile, 0644);
      if (fd != -1) {
        DLOG("mkfifo success\n");
        dzen_show_volume(fd, volume_adjust);
      } else if (errno == EEXIST) {
        DLOG("mkfifo lost race\n");
        int fd = open(kPipeFile, O_RDWR);
        dzen_show_volume(fd, volume_adjust);
      } else {
        // TODO(btolsch): real error
      }
    } else {
      struct stat st;
      if (fstat(fd, &st) == 0) {
        if (S_ISFIFO(st.st_mode)) {
          DLOG("found fifo\n");
          dzen_show_volume(fd, volume_adjust);
        }
      }
    }
  }
  return 0;
}
